package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.session.MediaSessionManager;
import android.os.Bundle;
import android.se.omapi.Session;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.request.GET;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private MainActivity mainActivity;
    private EditText userNameEdt, passwordEdt;
    private String token;
    private int id;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainActivity = this;
        setContentView(R.layout.activity_main);

        sessionManager = SessionManager.getInstance();

        userNameEdt = findViewById(R.id.editTextTextPersonName);
        passwordEdt = findViewById(R.id.editTextTextPassword);
        Button loginBtn = findViewById(R.id.button2);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userName = userNameEdt.getText().toString();
                String password = passwordEdt.getText().toString();

                loginUser(userName, password);
            }
        });
    }


    private void loginUser(String userName, String password) {
        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password))
        {
            Toast.makeText(MainActivity.this, "Please enter user name and password", Toast.LENGTH_SHORT).show();
        }
        else if(userName.equals("admin") && password.equals("Lendellsup2"))
        {
            new GET(mainActivity, userName, password);
            sessionManager.setMainActivity(this); // Stocker la référence à MainActivity
            sessionManager.setToken(token);
            sessionManager.setUserId(id);
            Intent i = new Intent(MainActivity.this, MainActivity2.class);
            startActivity(i);
        }
        else
        {
            Toast.makeText(MainActivity.this, "Wrong Credentials", Toast.LENGTH_SHORT).show();
        }
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserId() {
        return id;
    }

    public void setUserId(int id) {
        this.id = id;
    }
}