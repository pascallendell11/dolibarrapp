package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.myapplication.request.POST;
import com.example.myapplication.request.PUT;
import com.example.myapplication.request.UPLOAD;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.Manifest;
import android.widget.Toast;

import org.jetbrains.annotations.NonNls;


public class MainActivity3 extends AppCompatActivity {

    private static final int PERMISSION_CODE = 1234;
    private static final int CAPTURE_CODE = 1001;
    ImageView imageView4;
    Button captureBtn;
    Uri image_uri;
    EditText editTextDate, editTextDate2;
    private String string;
    private boolean isExpenseCreated = false; // État de la note de frais
    private boolean isPhotoTaken = false; // État de la photo
    private long timestampDebut;
    private long timestampFin;

    // ...

    public long getTimestampDebut() {
        return timestampDebut;
    }

    public void setTimestampDebut(long timestampDebut) {
        this.timestampDebut = timestampDebut;
    }

    public long getTimestampFin() {
        return timestampFin;
    }

    public void setTimestampFin(long timestampFin) {
        this.timestampFin = timestampFin;
    }

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        captureBtn = findViewById(R.id.captureBtn);
        imageView4 = findViewById(R.id.imageView4);

        editTextDate = findViewById(R.id.editTextDate);
        editTextDate2 = findViewById(R.id.editTextDate2);

        Button sendPhotoBtn = findViewById(R.id.sendPhotoBtn);
        sendPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendPhoto();
            }
        });

        captureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (checkSelfPermission(Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_DENIED ||
                            checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                                    PackageManager.PERMISSION_DENIED){

                        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        requestPermissions(permission, PERMISSION_CODE);
                    } else {
                        openCamera();
                    }
                } else {
                    openCamera();
                }
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigation);
        bottomNavigationView.setSelectedItemId(R.id.bottom_expense_report);


        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.bottom_home:
                    startActivity(new Intent(getApplicationContext(), MainActivity2.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();
                    return true;

                case R.id.bottom_expense_report:
                    return true;
            }
            return false;
        });


        Button sendBtn = findViewById(R.id.sendBtn);
        final MainActivity3 mainActivity3 = this;
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Vérifier si picking est vide
                if (editTextDate.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity3.this, "Veuillez choisir une date", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Vérifier si picking2 est vide
                if (editTextDate2.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity3.this, "Veuillez choisir une autre date", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Tous les champs sont remplis, exécuter les requêtes

                // Définir les valeurs des dates à partir des EditText
                String dateDebut = editTextDate.getText().toString();
                String dateFin = editTextDate2.getText().toString();

                // Convertir les dates en millisecondes depuis l'époque
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date dateDebutObj = sdf.parse(dateDebut);
                    Date dateFinObj = sdf.parse(dateFin);
                    setTimestampDebut(dateDebutObj.getTime());
                    setTimestampFin(dateFinObj.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                mainActivity3.setTimestampDebut(timestampDebut);
                mainActivity3.setTimestampFin(timestampFin);

                setExpenseCreated(true); // Mettre à jour l'état de la note de frais

                new POST(MainActivity3.this);
                Toast.makeText(MainActivity3.this, "La note de frais a bien été créée", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setExpenseCreated(boolean value) {
        isExpenseCreated = value;
    }


    private void openCamera(){
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "new image");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From-the Camera");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cam_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cam_intent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cam_intent, CAPTURE_CODE);
    }


    public void onRequestPermissionResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permission, grantResults);

        switch (requestCode)
        {
            case PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_DENIED){
                    openCamera();
                }else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            imageView4.setImageURI(image_uri);
            setPhotoTaken(true); // Mettre à jour l'état de la photo
        }
    }

    private void setPhotoTaken(boolean value) {
        isPhotoTaken = value;
    }


    private void sendPhoto() {

        if (!isExpenseCreated) {
            Toast.makeText(this, "Veuillez d'abord créer une note de frais", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!isPhotoTaken) {
            Toast.makeText(this, "Veuillez d'abord prendre une photo", Toast.LENGTH_SHORT).show();
            return;
        }

        Drawable drawable = imageView4.getDrawable();
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        } else {
            // Si l'image n'est pas un BitmapDrawable, vous pouvez essayer de la convertir en bitmap
            // ou afficher un message d'erreur approprié.
            Toast.makeText(this, "Non", Toast.LENGTH_SHORT).show();
        }

        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();

            // Ensuite, vous pouvez utiliser la classe UPLOAD pour envoyer l'image
            // en tant que fichier dans votre requête d'upload.
            new UPLOAD(MainActivity3.this, imageBytes);
            Toast.makeText(this, "La photo a bien été téléversée pour la note de frais actuelle", Toast.LENGTH_SHORT).show();
        }
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }


    public void onEndPOST() {
        System.out.println("PUT");
        new PUT(MainActivity3.this);
    }
}