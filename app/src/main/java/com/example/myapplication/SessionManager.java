package com.example.myapplication;

public class SessionManager {
    private static SessionManager instance;

    private MainActivity mainActivity;
    private String token;
    private int userId;

    private SessionManager() {
        // Constructeur privé pour empêcher l'instanciation directe
    }

    public static synchronized SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }
        return instance;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}