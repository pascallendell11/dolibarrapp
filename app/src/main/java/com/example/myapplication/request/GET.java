package com.example.myapplication.request;

import com.example.myapplication.MainActivity;
import com.example.myapplication.MainActivity2;
import com.example.myapplication.SessionManager;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GET extends Thread {

    private final MainActivity mainActivity;
    private final String userName;
    private final String password;

    public GET(MainActivity mainActivity, String userName, String password) {
        this.mainActivity = mainActivity;
        this.userName = userName;
        this.password = password;
        this.start();
    }

    @Override
    public void run() {
        try {
            URL url = new URL("https://dolibarr.lendell.engineer/api/index.php/login?login=" + userName + "&password=" + password);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            urlConnection.connect();
            BufferedReader buffer = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String ligne = "", resultat = buffer.readLine();
            while (ligne != null) {
                resultat += ligne + "\n";
                ligne = buffer.readLine();
            }
            System.out.println(resultat);
            JSONObject json = new JSONObject(resultat);
            json = json.getJSONObject("success");
            String token = json.getString("token");

            // Effectuer la deuxième requête GET uniquement si le token est obtenu avec succès
            if (!token.isEmpty()) {
                URL url1 = new URL("https://dolibarr.lendell.engineer/api/index.php/users/info");
                HttpURLConnection urlConnection1 = (HttpURLConnection) url1.openConnection();
                urlConnection1.setRequestMethod("GET");
                urlConnection1.setRequestProperty("Accept", "application/json");
                urlConnection1.setRequestProperty("DOLAPIKEY", token);

                urlConnection1.connect();
                BufferedReader buffer1 = new BufferedReader(new InputStreamReader(urlConnection1.getInputStream()));
                InputStream in1 = new BufferedInputStream(urlConnection1.getInputStream());
                String ligne1 = "", resultat1 = buffer1.readLine();
                while (ligne1 != null) {
                    resultat1 += ligne1 + "\n";
                    ligne1 = buffer1.readLine();
                }
                System.out.println(resultat1);
                JSONObject json1 = new JSONObject(resultat1);
                int id = json1.getInt("id");
                SessionManager.getInstance().setUserId(id);
            }

            SessionManager.getInstance().setToken(token);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}