package com.example.myapplication.request;

import android.annotation.SuppressLint;

import com.example.myapplication.MainActivity;
import com.example.myapplication.MainActivity3;
import com.example.myapplication.SessionManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class POST extends Thread
{
    private StringBuilder response;
    private MainActivity3 mainActivity3;
    private SessionManager sessionManager;

    public POST(MainActivity3 mainActivity3)
    {
        this.mainActivity3 = mainActivity3;
        sessionManager = SessionManager.getInstance();
        this.start();
    }

    @Override
    public void run()
    {
        try
        {
            URL url = new URL("https://dolibarr.lendell.engineer/api/index.php/expensereports");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("DOLAPIKEY", sessionManager.getToken());
            urlConnection.setDoOutput(true);

            long timestampDebut = mainActivity3.getTimestampDebut() / 1000;
            long timestampFin = mainActivity3.getTimestampFin() / 1000;

            String jsonInputString = "{\"fk_user_author\": "+sessionManager.getUserId()+", \"date_debut\": "+timestampDebut+", \"date_fin\": "+timestampFin+"}";
            System.out.println(jsonInputString);
            try(OutputStream os = urlConnection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
                response = new StringBuilder();
                String responseLine = "", line = "";
                while ((line = br.readLine()) != null) {
                    responseLine = responseLine + line;
                }
                mainActivity3.setString(responseLine);
                mainActivity3.onEndPOST();
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
