package com.example.myapplication.request;

import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.MainActivity2;
import com.example.myapplication.MainActivity3;
import com.example.myapplication.R;
import com.example.myapplication.SessionManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PUT extends Thread
{
    private final MainActivity3 mainActivity3;
    private SessionManager sessionManager;
    EditText editTextNumber;

    public PUT(MainActivity3 mainActivity3)
    {
        this.mainActivity3 = mainActivity3;
        sessionManager = SessionManager.getInstance();
        this.start();
    }

    @Override
    public void run()
    {
        editTextNumber = mainActivity3.findViewById(R.id.editTextNumber);

        if (editTextNumber.getText().toString().isEmpty()) {
            mainActivity3.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(mainActivity3, "Veuillez saisir un prix", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }

        try
        {
            URL url = new URL("https://dolibarr.lendell.engineer/api/index.php/expensereports/"+mainActivity3.getString());
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("PUT");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("DOLAPIKEY", sessionManager.getToken());
            urlConnection.setDoOutput(true);

            String jsonInputString = "{\"total_ttc\": "+editTextNumber.getText()+"}";
            System.out.println(jsonInputString);
            try(OutputStream os = urlConnection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = "", line = "";
                while ((line = br.readLine()) != null) {
                    responseLine = responseLine + line;
                }
                System.out.println(responseLine);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}