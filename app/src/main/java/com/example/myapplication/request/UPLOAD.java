package com.example.myapplication.request;

import com.example.myapplication.MainActivity3;
import com.example.myapplication.SessionManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;


public class UPLOAD extends Thread {
    private final MainActivity3 mainActivity3;
    private SessionManager sessionManager;
    private byte[] imageBytes;

    public UPLOAD(MainActivity3 mainActivity3, byte[] imageBytes) {
        this.mainActivity3 = mainActivity3;
        this.imageBytes = imageBytes;
        sessionManager = SessionManager.getInstance();
        this.start();
    }

    @Override
    public void run() {
        try {
            URL url = new URL("https://dolibarr.lendell.engineer/api/index.php/documents/upload");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("DOLAPIKEY", sessionManager.getToken());
            urlConnection.setDoOutput(true);

            String refValue = "(PROV" + mainActivity3.getString() + ")";
            String base64Image = Base64.getEncoder().encodeToString(imageBytes);

            String jsonInputString = "{\"filename\": \"image.jpg\", \"modulepart\": \"expensereport\", \"ref\": \"" +
                    refValue + "\", \"filecontent\": \"" + base64Image + "\", \"fileencoding\": \"base64\", \"overwriteifexists\": 1, \"createdirifnotexists\": 1}";
            System.out.println(jsonInputString);

            byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
            try (OutputStream os = urlConnection.getOutputStream()) {
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}